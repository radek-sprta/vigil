# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2018-02-09

### Added

- Notify user when Vigil is not properly configured.
- Enable notifying about changes through desktop notifications.

### Changed

- Obfuscate password in configuration file.
- Improve command line interface.
- Run Vigil asynchronously.
- Improve notifications.

### Fixed

- Fix broken cache path.
- Fix configuration checking.
- Access should get properly updated.
- Fix showing configuration.
- Notification should be sent again when it fails.
- Do not crash when trying to show non-existing website.

### Removed

- Remove support for Python 3.3 and 3.4.

## [0.2.0] - 2017-02-07

### Added

- Add interactive mode to command-line interface. Start it by `vigil -i`.
- Allow notifications to be send one by one, instead of in bulk.
- Implement proper logging.
- Add support for localization.
- Automatically detect character encoding of the watches sites.

### Changed

- Switch to TinyDB for storage.
- Improve the command line interface.
- Clean up the diff output. Notification should now send only the updated parts of a website, without any cruft.
- Switch to httplib2 for smarter http requests.
  
## 0.1.0 - 2016-12-30

- Initial release

[0.2.0]: https://gitlab.com/radek-sprta/vigil/compare/v0.1.1...v0.2.0
[0.3.0]: https://gitlab.com/radek-sprta/vigil/compare/v0.2.0...v0.3.0
