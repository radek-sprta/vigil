# Usage

Vigil has command line interface with the following commands:
* config
* add
* remove
* show
* check

You can check them by running:

`vigil --help`

Let's take a look at them one by one.

## Config
The config command is explained in the configuration section.

## Add
Add a new site to be watched.

`vigil add 'name' 'url'`

With `-t` flag you can specify in percents how much of the website must change to
to consider it updated. The default is 2%.

`vigil add 'name' 'url' -t 10`

With -i flag you can specify in hours how often should the site be checked.
The default is 24 hours.

`vigil add 'name' 'url' -i 2

## Remove
Remove a watch from the collection.

`vigil remove 'name'`

or `vigil remove 'url'`

## Show
Show the details about a watch, such as the last update time and update interval.

`vigil show 'name'`

or `vigil show 'url'`

To show a list of all watched sites, use:

`vigil show --all`

## Check
Check all sites in the collection and send a notification if they were sent.
    
`vigil check`

With the force option, yoy can check the watched sites before the interval runs out:

`vigil check --force`

## Interactive mode
You can run Vigil in interactive mode to save yourself some typing. It is
activated by using the `-i` flag. You can exit it by issuing the `quit` command
or using `Ctrl+D`.

```
$ vigil -i
vigil> vigil show -a
vigil> vigil check
vigil> quit
```

Is uses GNU Readline, which provides command history among other things. 
