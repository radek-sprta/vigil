# Configuration
Before using Vigil, you have to configure it first. To get a list
of all configuration options, run:

`vigil config --show

To change the default configuration options, use the following vigil
command:

`vigil config 'key' 'value'`

## Mail Notifications
To be able to receive notifications by email, you have to set the following
configuration options:
  * from - email adress to send from
  * to - email address to send to
  * smtp_host - address of the smtp host
  * smtp_port - smtp port, typically 465
  * smtp_username - username to log into the smtp server
  * smtp_password - password to log into the smtp server

## Desktop notification
If you just want to be notified by desktop notifications, change the output option:

`vigil config output notification`
  
## Manual configuration
You can also configure Vigil by directly editing the configuration file. First, Vigil with any option, it will create a default config file, if does not find any. If you installed Vigil from Git, it should be in you home directory.

`$HOME/.config/vigil/config.yaml`

If you install Vigil using snap, it will be in your snap home directory:

`$HOME/snap/vigi/$VERSION/.config/vigil/config.yaml`
