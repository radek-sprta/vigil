# Install

## Snap
Vigil is available as a snap package. On Ubuntu 14.04, 16.04 or later you can
install it using the following command:

`sudo snap install vigil`

## Git
Alternatively, you can get the most recent version from Gitlab. 
First clone the repository:

`git clone git://gitlab.com/radek-sprta/vigil.git`

And then install Vigil via pip:

`pip install -e .`
