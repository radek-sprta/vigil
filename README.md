# Vigil

Vigil is a command line application for watching sites for changes. When the site changes,
Vigil automatically sends user an email with the changes.
Perfect for checking that career page of the company you would really like to work for.


## Install
Vigil is available as a snap package. On Ubuntu 14.04, 16.04 or later you can
install it using the following command:

`sudo snap install vigil`

## Usage
Use `vigil -h` to get a list of commands and their options. I suggest starting by running `vigil config --show` and then configuring the options.

For further help, refer to the [documentation](https://radek-sprta.gitlab.io/vigil/).
